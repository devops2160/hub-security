provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    exec {
      api_version = "client.authentication.k8s.io/v1alpha1"
      args        = ["eks", "get-token", "--cluster-name", data.aws_eks_cluster.cluster.name]
      command     = "aws"
    }
  }
}
resource "helm_release" "gitlab" {
  name       = "gitlab-runner"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"
  namespace  = "gitlab" 
  create_namespace = true
  values = [
    file(".\\manifests\\gitlab-runner\\values.yaml")
  ]
  set {
    name  = "gitlabUrl"
    value = var.gitlab_repo_url
  }
  set {
    name  = "runnerRegistrationToken"
    value = var.gitlab_reg_token
  }

}