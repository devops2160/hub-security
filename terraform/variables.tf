################## EKS network config ####################
variable "region" {
  description = "AWS Deployment region"
  default = "us-east-1"
}
variable "environment" {
  description = "env spcification"
  default = "hubsec"
}

variable "availability_zones" {
  default = ["us-east-1a","us-east-1b"]
}

variable "private_subnets_cidr" {
    default = ["10.0.1.0/24"]
}

variable "public_subnets_cidr" {
    default = ["10.0.0.0/24"]
}

################### gitlab runner vars ######################
variable "gitlab_reg_token" {
    default = "GR13489414QHAYeWR4JhWm_3GjrVT"
}
variable "gitlab_repo_url" {
    default = "https://gitlab.com/"
}