################# basic config ###################
terraform {
  backend "s3" {
    bucket = "hub-securuty-tf-backend"
    key    = "eks/terraform.tfstate"
    region = "us-east-1"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.20.0"
    }

    local = {
      source  = "hashicorp/local"
      version = "2.1.0"
    }

    null = {
      source  = "hashicorp/null"
      version = "3.1.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.1"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
    required_version = ">= 0.14"
}

################# VPC creation ###################

locals {
  cluster_name = "hubsec-eks"
}



################### The VPC ###################
resource "aws_vpc" "hubsec-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name        = "hubsec-vpc"
    Environment = "dev"
  }
}
################### Subnets ###################
################### Internet gateway for the public subnet ###################
resource "aws_internet_gateway" "ig" {
  vpc_id = "${aws_vpc.hubsec-vpc.id}"
  tags = {
    Name        = "${var.environment}-igw"
    Environment = "${var.environment}"
  }
}
################### Elastic IP for NAT ###################
resource "aws_eip" "nat_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.ig]
}
################### NAT ###################
resource "aws_nat_gateway" "nat" {
  allocation_id = "${aws_eip.nat_eip.id}"
  subnet_id     = "${element(aws_subnet.public_subnet.*.id, 0)}"
  depends_on    = [aws_internet_gateway.ig]
  tags = {
    Name        = "nat"
    Environment = "${var.environment}"
  }
}
################### Public subnet ###################
resource "aws_subnet" "public_subnet" {
  vpc_id                  = "${aws_vpc.hubsec-vpc.id}"
  count                   = "${length(var.public_subnets_cidr)}"
  cidr_block              = "${element(var.public_subnets_cidr,   count.index)}"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true
  tags = {
    Name        = "${var.environment}-${element(var.availability_zones, count.index)}-      public-subnet"
    Environment = "${var.environment}"
  }
}
################### Private subnet ###################
resource "aws_subnet" "private_subnet" {
  vpc_id                  = "${aws_vpc.hubsec-vpc.id}"
  count                   = "${length(var.private_subnets_cidr)}"
  cidr_block              = "${element(var.private_subnets_cidr, count.index)}"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = false
  tags = {
    Name        = "${var.environment}-${element(var.availability_zones, count.index)}-private-subnet"
    Environment = "${var.environment}"
  }
}
################### Routing table for private subnet ###################
resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.hubsec-vpc.id}"
  tags = {
    Name        = "${var.environment}-private-route-table"
    Environment = "${var.environment}"
  }
}
################### Routing table for public subnet ###################
resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.hubsec-vpc.id}"
  tags = {
    Name        = "${var.environment}-public-route-table"
    Environment = "${var.environment}"
  }
}
resource "aws_route" "public_internet_gateway" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.ig.id}"
}
resource "aws_route" "private_nat_gateway" {
  route_table_id         = "${aws_route_table.private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.nat.id}"
}
################### Route table associations ###################
resource "aws_route_table_association" "public" {
  count          = "${length(var.public_subnets_cidr)}"
  subnet_id      = "${element(aws_subnet.public_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}
resource "aws_route_table_association" "private" {
  count          = "${length(var.private_subnets_cidr)}"
  subnet_id      = "${element(aws_subnet.private_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.private.id}"
}

################# security groups creation ###################
resource "aws_security_group" "worker_group_mgmt_1" {
  name_prefix = "worker_group_mgmt_1"
  vpc_id      = aws_vpc.hubsec-vpc.id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
    ]
  }
}

resource "aws_security_group" "worker_group_mgmt_2" {
  name_prefix = "worker_group_mgmt_2"
  vpc_id      = aws_vpc.hubsec-vpc.id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "192.168.0.0/16",
    ]
  }
}

resource "aws_security_group" "all_worker_mgmt" {
  name_prefix = "all_worker_management"
  vpc_id      = aws_vpc.hubsec-vpc.id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
      "172.16.0.0/12",
      "192.168.0.0/16",
    ]
  }
}
################# eks creation ###################

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "18.21.0"
  cluster_name    = local.cluster_name
  cluster_version = "1.20"
  subnet_ids         = ["${element(aws_subnet.public_subnet.*.id, 0)}","${element(aws_subnet.private_subnet.*.id, 0)}"]

  vpc_id = aws_vpc.hubsec-vpc.id

  eks_managed_node_groups = {
    public = {
      subnets          = "${element(aws_subnet.public_subnet.*.id, 0)}"
      desired_capacity = 5
      max_capacity     = 10
      min_capacity     = 3

      instance_type = "t2.large"
      k8s_labels = {
        Environment = "public"
              }
    }
    private = {
      subnets          = "${element(aws_subnet.public_subnet.*.id, 0)}"
      desired_capacity = 5
      max_capacity     = 10
      min_capacity     = 2

      instance_type = "t2.large"
      k8s_labels = {
        Environment = "private"
      }
    }
  }
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

################# outputs ###################

output "cluster_id" {
  description = "EKS cluster ID."
  value       = module.eks.cluster_id
}

output "cluster_endpoint" {
  description = "Endpoint for EKS control plane."
  value       = module.eks.cluster_endpoint
}

output "cluster_security_group_id" {
  description = "Security group ids attached to the cluster control plane."
  value       = module.eks.cluster_security_group_id
}

output "region" {
  description = "AWS region"
  value       = "us-east-1"
}

output "cluster_name" {
  description = "Kubernetes Cluster Name"
  value       = local.cluster_name
}

output "vpc_id" {
  value = "${aws_vpc.hubsec-vpc.id}"
}