###### Install Argocd #############
data "kubectl_file_documents" "argocd" {
    content = file(".\\manifests\\argocd\\install_argocd.yml")
}

data "kubectl_file_documents" "namespace" {
    content = file(".\\manifests\\argocd\\namespace.yml")
}
data "kubectl_file_documents" "argocd-server-lb" {
    content = file(".\\manifests\\argocd\\argocd_server_lb.yml")
}

resource "kubectl_manifest" "argocd" {
    depends_on = [
      kubectl_manifest.namespace,
    ]
    count     = length(data.kubectl_file_documents.argocd.documents)
    yaml_body = element(data.kubectl_file_documents.argocd.documents, count.index)
    override_namespace = "argocd"
}

resource "kubectl_manifest" "namespace" {
    count     = length(data.kubectl_file_documents.namespace.documents)
    yaml_body = element(data.kubectl_file_documents.namespace.documents, count.index)
    override_namespace = "argocd"
}

resource "kubectl_manifest" "argocd-server-lb" {
    count     = length(data.kubectl_file_documents.argocd-server-lb.documents)
    yaml_body = element(data.kubectl_file_documents.argocd-server-lb.documents, count.index)
    override_namespace = "argocd"
}
